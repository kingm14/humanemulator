package newpackage;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kingm14
 */
import java.util.*;
import java.awt.*;
public class ClickPoint {
    private int x,y;
    private int waitTime;
    
    public ClickPoint(int newX, int newY, long newTime)           
    {
        x=newX;
        y=newY;
        waitTime=(int)newTime;
    }
    public ClickPoint(int newX, int newY, int newTime) 
    {
        x=newX;
        y=newY;
        waitTime=newTime;
    }
    public int getX()
    {
        return x;
    }
    public int getY()
    {
        return y;
    }
    public int getWaitTime()
    {
        return waitTime;
    }
    public String toString()
    {
        String s="";
        s=waitTime+"...("+x+","+y+")";
        return s;
    }
            
        
    
    
}
