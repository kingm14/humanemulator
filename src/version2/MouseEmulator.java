package version2;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kingm14
 */
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.JLabel;
import java.awt.event.*;
import java.awt.Robot;
public class MouseEmulator{
    private Robot robot;
    public MouseEmulator()
    {
        try {
            robot = new Robot();
        } catch (AWTException ex) {
            Logger.getLogger(MouseEmulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void moveTo(int x, int y, int speed)
    {
        PointerInfo a = MouseInfo.getPointerInfo();
        Point b = a.getLocation();
        int curX = (int) b.getX();
        int curY = (int) b.getY();
        //System.out.print(curX+",");
        //System.out.println(curY);
        if(Math.abs(x-curX)<5&&Math.abs(y-curY)<5)
        {
            //System.out.println("base Case");
            robot.mouseMove(x, y);
            return;
        }
        else if(Math.abs(x-curX)>Math.abs(y-curY))
        {
            //System.out.println("X Distance Greater");
            if(x>curX)robot.mouseMove(curX+2, curY);
            else robot.mouseMove(curX-2, curY);
        }
        else //if(Math.abs(x-curX)<=Math.abs(y-curY))
        {
            //System.out.println("Y Distance Greater");
            if(y>curY)robot.mouseMove(curX, curY+2);
            else robot.mouseMove(curX, curY-2);
        }
        int distance=Math.abs(x-curX)+Math.abs(y-curY);
        int wait=distance/2;
        wait=speed/wait;
        //System.out.println(wait);
        //System.out.println(MouseInfo.getPointerInfo().getLocation().getX()+","+MouseInfo.getPointerInfo().getLocation().getY());
        robot.delay(wait);
        moveTo(x,y,speed-wait);
    }
    public void jumpTo(int x, int y)
    {
        robot.mouseMove(x, y);
    }
    public void click()
    {
        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }
    public void release()
    {
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }
    public void wait(int i){robot.delay(i);}
    
}
