package version2;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kingm14
 */
import java.util.*;
import java.awt.*;
public class ClickPoint {
    private int x,y;
    private int waitTime;
    
    public ClickPoint(int newX, int newY, long newTime)           
    {
        x=newX;
        y=newY;
        waitTime=(int)newTime;
    }
    public ClickPoint(int newX, int newY, int newTime) 
    {
        x=newX;
        y=newY;
        waitTime=newTime;
    }
    public ClickPoint(String s)
    {
        Integer a=new Integer("5");
        //System.out.println(convertToInt("5"));
        String temp=s.substring(0/*s.indexOf("[")+1*/,s.indexOf("|"));
        //System.out.println(temp);
        //Integer i=new Integer(temp);
        waitTime=convertToInt(s.substring(0/*s.indexOf("[")+1*/,s.indexOf("|")));
                //i.intValue();
        //i=new Integer(s.substring(s.indexOf("(")+1,s.indexOf(",")));
        x=convertToInt(s.substring(s.indexOf("(")+1,s.indexOf(",")));
        y=convertToInt(s.substring(s.indexOf(",")+1,s.indexOf(")")));
    }
    private int convertToInt(String s)
    {
        return (new Integer(s)).intValue();
    }
    public int getX()
    {
        return x;
    }
    public int getY()
    {
        return y;
    }
    public int getWaitTime()
    {
        return waitTime;
    }
    public String toString()
    {
        String s="";
        s="["+waitTime+"|("+x+","+y+")]";
        return s;
    }
            
        
    
    
}
