package version2;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kingm14
 */
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.JLabel;
import java.awt.event.*;
import java.awt.Robot;
public class KeyEmulator {
    private Robot robot;
    public KeyEmulator()
    {
        try {
            robot=new Robot();
        } catch (AWTException ex) {
            Logger.getLogger(KeyEmulator.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(getKeyCode('a'));
        System.out.println(getKeyCode('0'));
        
    }
    public void type(String s)
    {
        char[] arr = s.toCharArray();
        for(int i=0; i<arr.length; i++)
        {
            int kcode = getKeyCode(arr[i]);
            robot.keyPress(kcode);
            robot.keyRelease(kcode);
        }

    }
    public void type(int kcode)
    {
        robot.keyPress(kcode);
        robot.keyRelease(kcode);
    }
    public static int getKeyCode(char c)
    {
        //System.out.println((int)c);
        String s=""+c;
        return getKeyCode(s);
        //return ((int)s.toUpperCase().charAt(0));
    }
    public static int getKeyCode(String s)
    {
        return ((int)s.toUpperCase().charAt(0));
        
    }

    public void wait(int i){robot.delay(i);}
    public static void main(String[] args)
    {
       System.out.println(getKeyCode('a'));
       System.out.println(getKeyCode('0')); 
    }
}
