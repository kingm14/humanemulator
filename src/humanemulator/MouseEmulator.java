package humanemulator;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kingm14
 */
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.JLabel;
import java.awt.event.*;
import java.awt.Robot;
public class MouseEmulator {
    private Robot mouse;
    public MouseEmulator()
    {
        try {
            mouse = new Robot();
        } catch (AWTException ex) {
            Logger.getLogger(MouseEmulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void moveTo(int x, int y, int speed)
    {
        PointerInfo a = MouseInfo.getPointerInfo();
        Point b = a.getLocation();
        int curX = (int) b.getX();
        int curY = (int) b.getY();
        System.out.print(curX+",");
        System.out.println(curY);
        if(Math.abs(x-curX)<speed&&Math.abs(y-curY)<speed)
        {
            System.out.println("base Case");
            mouse.mouseMove(x, y);
            return;
        }
        else if(Math.abs(x-curX)>Math.abs(y-curY))
        {
            System.out.println("X Distance Greater");
            if(x>curX)mouse.mouseMove(curX+speed, curY);
            else mouse.mouseMove(curX-speed, curY);
        }
        else //if(Math.abs(x-curX)<=Math.abs(y-curY))
        {
            System.out.println("Y Distance Greater");
            if(y>curY)mouse.mouseMove(curX, curY+speed);
            else mouse.mouseMove(curX, curY-speed);
        }
        
        System.out.println(MouseInfo.getPointerInfo().getLocation().getX()+","+MouseInfo.getPointerInfo().getLocation().getY());
        mouse.delay(5);
        moveTo(x,y,speed);
    }
    public void jumpTo(int x, int y)
    {
        mouse.mouseMove(x, y);
    }
    public void click()
    {
        mouse.mousePress(InputEvent.BUTTON1_MASK);
        mouse.mouseRelease(InputEvent.BUTTON1_MASK);
    }
    
}
