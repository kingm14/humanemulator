package newpackage3;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kingm14
 */
import java.util.*;
import java.awt.*;
import java.lang.reflect.*;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ClickPoint {
    private int x,y;
    private int waitTime;
    private String type;
    private boolean isMethodCall;
    private Class c;
    private Method m;
    private boolean isLeftClick;
    public ClickPoint(int newX, int newY, long newTime)           
    {
        x=newX;
        y=newY;
        waitTime=(int)newTime;
        type="";
        isMethodCall=false;
    }
    public ClickPoint(int newX, int newY, int newTime) 
    {
        x=newX;
        y=newY;
        waitTime=newTime;
        type="";
        isMethodCall=false;
    }
    public ClickPoint(int newX, int newY, long newTime, String s)
    {
        x=newX;
        y=newY;
        waitTime=(int)newTime;
        type=s;
        isMethodCall=false;
    }
    public ClickPoint(int newX, int newY, long newTime, int i)
    {
        x=newX;
        y=newY;
        waitTime=(int)newTime;
        isMethodCall=false;
       
        //System.out.println("127="+(char)127);
        //System.out.println(i+"="+ (new Character((char)i)));
       
        type=convertToString(i);
    }
    public ClickPoint(String s)
    {
        Integer a=new Integer("5");
        //System.out.println(convertToInt("5"));
        String temp=s.substring(0/*s.indexOf("[")+1*/,s.indexOf("|"));
        //System.out.println(temp);
        //Integer i=new Integer(temp);
        waitTime=convertToInt(s.substring(0/*s.indexOf("[")+1*/,s.indexOf("|")));
                //i.intValue();
        //i=new Integer(s.substring(s.indexOf("(")+1,s.indexOf(",")));
        x=convertToInt(s.substring(s.indexOf("(")+1,s.indexOf(",")));
        y=convertToInt(s.substring(s.indexOf(",")+1,s.indexOf(")")));
        type=s.substring(s.indexOf("\"")+1,s.lastIndexOf("\""));
        if(type.equals("Θ"))
        {
            isLeftClick=true;
            type=null;
        }
        
    
        
    }
    
    public boolean isMethodCall()
    {
        return isMethodCall;
    }
    public void setIsMethodCall(boolean b)
    {
        isMethodCall=b;
    }
    public void setIsLeftClick(boolean b)
    {
        isLeftClick=b;
    }
    public boolean isLeftClick()
    {
        return isLeftClick;
    }
    public void setMethod(String className, String methodCall)
    {
        try {
            
            c=Class.forName(className);
            try {
                m=c.getMethod(methodCall.substring(0,methodCall.indexOf("(")),
                        Class.forName(methodCall.substring(methodCall.indexOf("(")+1,methodCall.indexOf(" "))));
                System.out.println(m.getName());
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(ClickPoint.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecurityException ex) {
                Logger.getLogger(ClickPoint.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClickPoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean isTypePoint()
    {
        if(type.equals("")) return false;
        return true;
    }
    public static String convertToString(int i)
    {
        String s="";
        //System.out.println("127="+(char)127);
        //System.out.println(i+"="+ (new Character((char)i)));
        if(i==8)
        {
            s+="<-";
        }
        else if(i==16)
        {
            s+="Shift";
        }
        else if(i==38)
        {
            s+="Up";
        }
        else if(i==40)
        {
            s+="Down";
        }
        else if(i==37)
        {
            s+="Left";
        }
        else if(i==39)
        {
            s+="Right";
        }
        else
        {
            s+=(char)i;
        }
        return s;
    }
    
    private int convertToInt(String s)
    {
        return (new Integer(s)).intValue();
    }
    public int getX()
    {
        return x;
    }
    public int getY()
    {
        return y;
    }
    public int getWaitTime()
    {
        return waitTime;
    }
    public static int decode(String s)
    {
        if(s.equals("<-"))
        {
            return 8;
        }
        else if(s.equals("Shift"))
        {
            return 16;
        }
        else if(s.equals("Up"))
        {
            return 38;
        }
        else if(s.equals("Down"))
        {
            return 40;
        }
        else if(s.equals("Left"))
        {
            return 37;
        }
        else if(s.equals("Right"))
        {
            return 39;
        }
        //.equals("˂")||type.equals("˅")||type.equals("˄")||type.equals("˃")
        else if(s.equals("˂"))
        {
            return 37;
        }
        else if(s.equals("˅"))
        {
            return 40;
        }
        else if(s.equals("˄"))
        {
            return 38;
        }
        else if(s.equals("˃"))
        {
            return 39;
        }
        
        else
        {
            return s.charAt(0);
        }
    }
    public String getTyped()
    {
        return type;
    }
    public String getReleaseTyped()
    {
        if(!isRelease()){
            return getTyped();
        }
        return type.substring(1);
    }
    
    public int getTypedVal()
    {
        return decode(getTyped());
    }
    public int getReleaseVal()
    {
        if(isRelease())
        {
            return decode(getReleaseTyped());
        }
        return getTypedVal();
    }
    public String toFormatString()
    {
        String s="";
        if(isLeftClick())
        {
            s="["+waitTime+"|("+x+","+y+")(left click)]";
        }
        else if(isRelease())
        {
            s="["+waitTime+"| release]";
        }
        else if(type.equals("")){
            s="["+waitTime+"|("+x+","+y+")]";
        }
        
        else if(!isClick())
        {
            s="["+waitTime+"|\""+type+"\"]";
        }
        else 
        {
            s="["+waitTime+"|("+x+","+y+")\""+type+"\"]";
        }
        return s;
    }
    public String toString()
    {
        if(isLeftClick())
        {
            return "["+waitTime+"|("+x+","+y+")\""+"Θ"+"\"]";
        }
        String s="";
        s="["+waitTime+"|("+x+","+y+")\""+type+"\"]";
        return s;
    }
    public String toSaveString()
    {
        if(isLeftClick())
        {
            return "["+waitTime+"|("+x+","+y+")\""+"Θ"+"\"]";
        }
        String s="";
        s="["+waitTime+"|("+x+","+y+")\""+decode(type)+"\"]";
        return s;
    }
    public void setX(int newX)
    {
        x=newX;
    }
    public void setY(int newY)
    {
        y=newY;
    }
    public void setWait(int newWait)
    {
        waitTime=newWait;
    }
    public void setTyped(String s)
    {
        type= s;
    }
    public boolean isArrowKey()
    {
        //"˂˄˃˅"
        if(type.equals("˂")||type.equals("˅")||type.equals("˄")||type.equals("˃"))
        {
            return true;
        }
        return false;
    }
    public boolean isClick()
    {
        return !(x==-1||y==-1);
    }
    public boolean isRelease()
    {
        return type.indexOf("ֆ")!=-1;
    }
          
    
            
        
    
    
}
