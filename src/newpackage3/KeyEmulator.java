package newpackage3;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kingm14
 */
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.JLabel;
import java.awt.event.*;
import java.awt.Robot;
public class KeyEmulator {
    private Robot robot;
    private boolean testing;
    public KeyEmulator()
    {
        testing=false;
        try {
            robot=new Robot();
        } catch (AWTException ex) {
            Logger.getLogger(KeyEmulator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //if(testing) System.out.println(getKeyCode('a'));
        //if(testing) System.out.println(getKeyCode('0'));
        
        
    }
    public void type(String s)
    {
        
        char[] arr = s.toCharArray();
        for(int i=0; i<arr.length; i++)
        {
            //up=38 down=40 left=37 right=39
            if(s.substring(i, i+1).equals("˂")){type(37);}
            else if(s.substring(i, i+1).equals("˄")){type(38);}
            else if(s.substring(i, i+1).equals("˃")){type(39);}
            else if(s.substring(i, i+1).equals("˅")){type(40);}
            else{
            
                if(s.substring(i, i+1).toUpperCase().equals(s.substring(i, i+1))&&!Character.isDigit(s.charAt(i)))
                {
                    robot.keyPress(16);
                }

                int kcode = getKeyCode(arr[i]);
                type(kcode);
                if(s.substring(i, i+1).toUpperCase().equals(s.substring(i, i+1))&&!Character.isDigit(s.charAt(i)))
                {
                    robot.keyRelease(16);
                }
            }
            
        }

    }
    public void type(int kcode)
    {
        if(testing) System.out.println("Typed: "+(char)kcode);
        robot.keyPress(kcode);
        robot.keyRelease(kcode);
    }
    public void release(int kcode)
    {
        robot.keyRelease(kcode);
    }
    public void press(int kcode)
    {
        robot.keyPress(kcode);
    }
    public static int getKeyCode(char c)
    {
        //if(testing) System.out.println((int)c);
        String s=""+c;
        return getKeyCode(s);
        //return ((int)s.toUpperCase().charAt(0));
    }
    public static int getKeyCode(String s)
    {
        return ((int)s.toUpperCase().charAt(0));
        
    }

    public void wait(int i){robot.delay(i);}
    public static void main(String[] args)
    {
       // System.out.println(getKeyCode('a'));
        //System.out.println(getKeyCode('0')); 
    }
    
}
