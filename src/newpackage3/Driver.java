package newpackage3;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * screenSize.java
 *
 * Created on Oct 4, 2011, 12:50:33 PM
 */
/**
 *
 * @author kingm14
 */
import java.awt.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.JLabel;
import java.awt.event.*;
import java.awt.Robot;
import java.util.*;
import java.io.*;
import java.lang.*;
import javax.swing.plaf.basic.BasicTreeUI.KeyHandler;
import java.lang.reflect.*;


public class Driver extends javax.swing.JFrame implements KeyListener, AWTEventListener {

    /** Creates new form screenSize */
    private MouseEmulator e;
    private ArrayList<ClickPoint> al;
    private LinkedList<ClickPoint> l;
    //private Iterator iter;
    private StopWatch watch;
    private DefaultListModel model;
    private JFrame frame;
    private boolean recording;
    private boolean onTop;
    private KeyEmulator kEmulator;
    private boolean typing;
    private boolean testing;
    private String arrowSymbols;
    private boolean isCanceled;
    private int prePauseTime;
    public Driver() {
        prePauseTime=0;
        isCanceled=false;
        initComponents();
        Toolkit.getDefaultToolkit().addAWTEventListener(this, AWTEvent.MOUSE_EVENT_MASK  | AWTEvent.KEY_EVENT_MASK| AWTEvent.FOCUS_EVENT_MASK);
        this.addKeyListener(this);
        e=new MouseEmulator();
        kEmulator=new KeyEmulator();
        al=new ArrayList<ClickPoint>();
        l=new LinkedList<ClickPoint>();
        //iter=l.listIterator(0);
        watch = new StopWatch();
        model = new DefaultListModel();
        recording=false;
        frame = new Open();
        onTop=true;
        //m.jumpTo(172, 112);
        
        list.setModel(model);
        jFileChooser1.setVisible(false);
        typing=false;
        typeField.grabFocus();
        testing=false;
        arrowSymbols="˂˄˃˅";
        jPanel1 = new javax.swing.JPanel() {

            protected void paintComponent(Graphics g) {
                if (g instanceof Graphics2D) {
                    final int R = 240;
                    final int G = 240;
                    final int B = 240;


                    Paint p =
                            new GradientPaint(0.0f, 0.0f, new Color(R, G, B, 0),
                            getWidth(), getHeight(), new Color(R, G, B, 255), true);
                    Graphics2D g2d = (Graphics2D) g;
                    g2d.setPaint(p);
                    g2d.fillRect(0, 0, getWidth(), getHeight());

                } else {
                    super.paintComponent(g);
                }
            }
        };
        this.add(jPanel1);
        
        
        
        

        //new java.awt.Dimension(((int) (new Toolkit.getDefaultToolkit()).getScreenSize().getWidth()),((int) (new Toolkit.getDefaultToolkit()).getScreenSize().getHeight()))
    }
    
    public void addClickPoint(ClickPoint clickPoint, int i)
    {
        al.add(i, clickPoint);
        model.add(i, clickPoint.toFormatString());
        if(testing)System.out.println("point added");
    }
    public void addClickPoint(ClickPoint clickPoint)
    {
        addClickPoint(clickPoint, al.size());
    }
    public void setClickPoint(ClickPoint clickPoint, int i)
    {
        al.set(i, clickPoint);
        model.setElementAt(clickPoint.toFormatString(), i);
    }
    public void removeClickPoint(int i)
    {
        al.remove(i);
        model.remove(i);
    }
    public void removeClickPoint()
    {
        removeClickPoint(al.size()-1);
    }
    
    public void eventDispatched(AWTEvent event) {
        //if(event.)
        //if(testing)  System.out.println(event.getID());
        //if(testing) System.out.println();
        
        int x,y;
        x=(int)MouseInfo.getPointerInfo().getLocation().getX();
        y=(int)MouseInfo.getPointerInfo().getLocation().getY();
        //if(testing)System.out.println(x+","+y);
       //
        //if(testing) System.out.println(event.getID());
        
        if(event.getID()==java.awt.Event.KEY_PRESS)
        {
            if(((KeyEvent)event).getKeyCode()==KeyEvent.VK_ESCAPE){
                isCanceled=true;
                //int orig=al.get(list.getSelectedIndex()).getWaitTime();
                //al.get(list.getSelectedIndex()).setWait(0);
                e.stop();
                //al.get(list.getSelectedIndex()).setWait(orig);
                System.out.println("cancled");
            }
            else if(((KeyEvent)event).getKeyCode()==KeyEvent.VK_ENTER&&((KeyEvent)event).isControlDown())
            {
                //addClickPoint(new ClickPoint(x,y,5));
                //addClickPoint(new ClickPoint(x,y,5));
                e.go();
                e.click();
                for(int i=0; i<5; i++)
                {
                    
                    e.moveTo(x+50, y, 15);
                    e.click();
                    e.moveTo(x, y, 15);
                    e.click();
                    
                    addClickPoint(new ClickPoint(x+50,y,15));
                    addClickPoint(new ClickPoint(x,y,15));
                    
                    
                }
                //refocus(x,y);
            }
            
        }
        
        if(recording){
            long time=0;
            
            if(event.getID()==java.awt.Event.KEY_PRESS&&(x>276||y>234))
                {
                    if(((KeyEvent)event).isShiftDown()||((KeyEvent)event).isAltDown()||((KeyEvent)event).isControlDown())return;
                    kEmulator.type(((KeyEvent)event).getKeyCode());
                    refocus(x,y);
                    typeField.grabFocus();
                    /*if(watch.isRunning())
                    {
                        watch.stop();
                        time=watch.getElapsedTime();
                    }*/
                    //if(((KeyEvent)event).getKeyCode()==8) typeField.setText(typeField.getText()+"#");
                    
                    //recording=false;
                    //onTop=false;
                    //kEmulator.type(8);
                    /*if(al.get(al.size()-1).isClick()){
                        e.jumpTo(x, y);
                        e.click();
                    }*/
                    //e.jumpTo(x, y);
                    //kEmulator.release(((KeyEvent)event).getKeyCode());
                    //up=38 down=40 left=37 right=39
                    
                    if(((KeyEvent)event).getKeyCode()>=37&&((KeyEvent)event).getKeyCode()<=40)
                    {
                        
                        typeField.setText(arrowSymbols.substring(((KeyEvent)event).getKeyCode()-37,((KeyEvent)event).getKeyCode()-36));
                        //typing=false;
                    }
                    
                    /*else{
                        typeField.setText(ClickPoint.convertToString(((KeyEvent)event).getKeyCode()));
                        e.click();
                        kEmulator.release(((KeyEvent)event).getKeyCode());
                        kEmulator.type(((KeyEvent)event).getKeyCode());
                    }*/
                    else{
                        typeField.setText(ClickPoint.convertToString(((KeyEvent)event).getKeyCode()));
                        //e.jumpTo(x, y);
                       // e.click();
                        //e.click();
                        //kEmulator.release(((KeyEvent)event).getKeyCode());
                        //kEmulator.type(((KeyEvent)event).getKeyCode());
                        
                    }
                    //e.jumpTo(x, y);
                    //kEmulator.press(((KeyEvent)event).getKeyCode());
                    typing=true;
                    if(testing) System.out.println(((KeyEvent)event).getKeyCode());
                    
                    
                    
                    //al.add(new ClickPoint(x,y,time,(((KeyEvent)event).getKeyCodeya
                    //model.add(model.getSize(), al.get(al.size()-1).toFormatString());
                    
                    if(testing) System.out.println("Key Pressed");
                    if(!al.get(al.size()-1).getTyped().equals(typeField.getText()))
                    {
                        if(watch.isRunning())
                        {
                            watch.stop();
                            time=watch.getElapsedTime();
                        }
                        addClickPoint(new ClickPoint(-1,-1,time, typeField.getText()));
                        list.ensureIndexIsVisible(al.size()-1);
                        watch.start();
                        
                    //refocus(x,y); 
                    //jSpinner1.setValue(1);
                     //recording=
                    //onTop=true;
                    //refocus(x,y);
                }
                    //if(!al.get(al.size()-1).isClick())
                    
                    typeField.setText("");
                    
                   
                    
                    
             
                    
            }
            else if(event.getID()==java.awt.Event.KEY_RELEASE&&(x>268||y>310))
            {
                //System.out.println("released");
                if(((KeyEvent)event).isShiftDown()||((KeyEvent)event).isAltDown()||((KeyEvent)event).isControlDown()||((KeyEvent)event).isActionKey()||textField.getText().equals(""))return;
                
                if(watch.isRunning())
                {
                    watch.stop();
                    time=watch.getElapsedTime();
                }
                addClickPoint(new ClickPoint(-1,-1,time,"ֆ"+ClickPoint.convertToString(((KeyEvent)event).getKeyCode())));
                //kEmulator.release(((KeyEvent)event).getKeyCode());
                watch.start();
                typeField.setText("");
                
            }
            
            else if(event.getID()==java.awt.Event.LOST_FOCUS&&(x>268||y>310))
            {
                
                //if(!(al.size()>0&&al.get(al.size()-1).isTypePoint()&&al.get(al.size()-1).getX()==x&&al.get(al.size()-1).getY()==y*/))
                //{
                    
                    if(watch.isRunning())
                    {
                        watch.stop();  
                        time=watch.getElapsedTime();
                        if(prePauseTime!=0)
                        {
                            time+=prePauseTime;
                            prePauseTime=0;
                        }
                    }
                    
                    
                    if(testing) System.out.println("Mouse Pressed");

                    ((MouseEmulator)e).release(); 
                    e.jumpTo(x, y);
                    e.click();
                    e.click();
                    
                    //e.moveTo(x, y+10,10);
                    //e.jumpTo(x, y);
                    e.click();
                    //e.wait(5);
                    
                    if(onTop)
                    {
                            refocus(x,y);
                            refocus(x,y);
                        
                        if(testing) System.out.println("Refocused");
                    }


                    //((MouseEmulator)e).release();
                    
                    /*if(typing)
                    {
                        typing=false;
                        //if(al.size()>0&&(al.get(al.size()-1).isTypePoint()/*||al.get(al.size()-1).isArrowKey()))
                        
                            /*x=-1;
                            y=-1;
                            addClickPoint(new ClickPoint(x,y,time, typeField.getText()));
                            typeField.setText("");
                            //model.add(model.getSize(), al.get(al.size()-1).toFormatString());
                        
                        /*if(al.size()>0&&al.get(al.size()-1).isTypePoint()&&!al.get(al.size()-1).isArrowKey())
                        {
                            /*if(al.get(al.size()-1).getTyped().charAt(al.get(al.size()-1).getTyped().length()-1)=='#')
                            {
                                al.get(al.size()-1).setTyped(al.get(al.size()-1).getTyped().substring(0,al.get(al.size()-1).getTyped().length()-1));
                            }
                            else{
                                al.get(al.size()-1).setTyped(typeField.getText());//al.get(al.size()-1).getTyped()+typeField.getText().charAt(typeField.getText().length()-1));
                            //typeField.setText("");
                            model.remove(model.getSize()-1);
                           
                        
                        }
                        else 
                        {
                            al.add(new ClickPoint(x,y,time, typeField.getText()));
                            //
                        }
                        model.add(model.getSize(), al.get(al.size()-1).toFormatString());*/
                        
                        
                    //}
                    //else{
                        addClickPoint(new ClickPoint(x,y,time));
                        list.ensureIndexIsVisible(al.size()-1);
                        typeField.setText("");
                        
                    //}
                    watch.start();
                    
                //}  
                


                    
                
            }
            
            
        }
        //if(testing) System.out.println(event);
        //if(!jPanel1.isFocusOwner())
            //if(testing)  System.out.println("works");
    }
    public void refocus(int x, int y)
    {
        
            ((MouseEmulator)e).jumpTo(5, 150);
            ((MouseEmulator)e).click();
            ((MouseEmulator)e).click();
            ((MouseEmulator)e).jumpTo(x, y);
        
        if(testing)System.out.println("refocused");
    }
    public void run(int numTimesRepeat, int sp1,int ep1)
    {
        e.go();
        final int numTimes=numTimesRepeat;
        final int sp=sp1;
        final int ep=ep1;
        class Run extends SwingWorker<Boolean,Boolean>
        {

            @Override
            protected Boolean doInBackground() {
                
                for(int j=0; j<numTimes;j++)
                {
                    System.out.println("Works");

                    /*while(iter.hasNext())
                    {
                        try{
                        ClickPoint cp=(ClickPoint)iter.next();
                        ((MouseEmulator)e).moveTo((int) cp.getX(), (int) cp.getY(), cp.getWaitTime());             
                        ((MouseEmulator)e).click();
                        }catch(java.util.ConcurrentModificationException exception){} 
                    }*/
                    for (int i = sp; i < ep; i++) {              
                        //m.wait(al.get(i).getWaitTime());
                        if(isCanceled)
                        {
                            return false;
                        }
                        list.setSelectedIndex(i);
                        list.ensureIndexIsVisible(i);
                        
                        repaint();
                        try {
                            Thread.sleep(5);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if(al.get(i).isClick()){
                            ((MouseEmulator)e).moveTo((int) al.get(i).getX(), (int) al.get(i).getY(), al.get(i).getWaitTime());             
                            ((MouseEmulator)e).click();
                            e.click();
                            e.click();
                            //((MouseEmulator)e).moveTo((int) al.get(i).getX()+5, (int) al.get(i).getY()+6, 5);   
                            //((MouseEmulator)e).moveTo((int) al.get(i).getX(), (int) al.get(i).getY(), 5);   
                            e.click();
                            //((MouseEmulator)e).moveTo((int) al.get(i).getX(), (int) al.get(i).getY(), 5);
                            if(al.size()-1!=i&&al.get(i+1).getWaitTime()>20){
                                refocus((int) al.get(i).getX(), (int) al.get(i).getY());
                            }
                        }
                        if(al.get(i).isTypePoint())
                        {
                            kEmulator.wait(al.get(i).getWaitTime());
                            if(al.get(i).isRelease())
                            {
                                kEmulator.release(al.get(i).getReleaseVal());
                            }
                            else
                            {
                                kEmulator.press(al.get(i).getTypedVal());
                            }
                        }
                        //refocus((int) al.get(i).getX(), (int) al.get(i).getY());
                    }
                    refocus((int) al.get(al.size()-1).getX(), (int) al.get(al.size()-1).getY());
                    
                    //((MouseEmulator)e).wait(1000);



                }
                return true;
            }
            
        }
        Run r=new Run();
        r.execute();
        
        isCanceled=false;
        //BasicTreeUI.KeyHandler ka=new BasicTreeUI.KeyHandler();
         
        
    }
            

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton4 = new javax.swing.JButton();
        jFrame1 = new javax.swing.JFrame();
        xVal = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        yVal = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        textField = new javax.swing.JTextField();
        doneButton = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        waitVal = new javax.swing.JSpinner();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        jFileChooser1 = new javax.swing.JFileChooser();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSpinner1 = new javax.swing.JSpinner();
        removeButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        findButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        list = new javax.swing.JList();
        typeField = new javax.swing.JTextField();
        typeButton = new javax.swing.JButton();
        isRecordingLabel = new javax.swing.JLabel();
        fileChooser = new javax.swing.JFileChooser();
        fileChooser2 = new javax.swing.JFileChooser();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        openMenuItem = new javax.swing.JMenuItem();
        saveMenuItem = new javax.swing.JMenuItem();
        importMenuItem = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        addMenuItem = new javax.swing.JMenuItem();
        addAtSelectedMenuItem = new javax.swing.JMenuItem();
        editSelectedMenuItem = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        clearMenuItem = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        runMenuItem = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        runSomeMenuItem = new javax.swing.JMenuItem();
        recordMenuItem = new javax.swing.JCheckBoxMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();

        jButton4.setText("jButton4");

        jFrame1.setAlwaysOnTop(true);
        jFrame1.setSize(new java.awt.Dimension(301, 205));

        jLabel3.setText("X:");

        jLabel4.setText("Y:");

        jLabel5.setText("Type:");

        doneButton.setText("Done");
        doneButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doneButtonActionPerformed(evt);
            }
        });

        jLabel6.setText("Wait:");

        org.jdesktop.layout.GroupLayout jFrame1Layout = new org.jdesktop.layout.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jFrame1Layout.createSequentialGroup()
                .add(jFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jFrame1Layout.createSequentialGroup()
                        .addContainerGap()
                        .add(doneButton))
                    .add(jFrame1Layout.createSequentialGroup()
                        .add(20, 20, 20)
                        .add(jFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jFrame1Layout.createSequentialGroup()
                                .add(jLabel6)
                                .add(1, 1, 1)
                                .add(waitVal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jFrame1Layout.createSequentialGroup()
                                .add(9, 9, 9)
                                .add(jLabel3)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(xVal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(37, 37, 37)
                                .add(jLabel4)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(yVal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jFrame1Layout.createSequentialGroup()
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, textField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
                                    .add(jLabel5))))))
                .addContainerGap())
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jFrame1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jFrame1Layout.createSequentialGroup()
                        .add(jFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jLabel6)
                            .add(waitVal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(18, 18, 18)
                        .add(jFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(xVal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel3)))
                    .add(yVal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel4))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel5)
                .add(6, 6, 6)
                .add(textField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(doneButton)
                .add(81, 81, 81))
        );

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setLocation(new java.awt.Point(0, 0));
        setResizable(false);

        jLabel1.setText("Repeat");

        jSpinner1.setValue(new Integer(1));

        removeButton.setText("Remove");
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Times");

        findButton.setText("Find Point");
        findButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                findButtonActionPerformed(evt);
            }
        });

        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setViewportView(null);

        list.setValueIsAdjusting(true);
        list.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                listCaretPositionChanged(evt);
            }
        });
        jScrollPane1.setViewportView(list);

        typeField.setForeground(new java.awt.Color(153, 153, 153));
        typeField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                typeFieldActionPerformed(evt);
            }
        });

        typeButton.setText("Type");
        typeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                typeButtonActionPerformed(evt);
            }
        });

        isRecordingLabel.setFont(new java.awt.Font("Futura", 0, 18));
        isRecordingLabel.setForeground(new java.awt.Color(0, 255, 0));
        isRecordingLabel.setText("Stop");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(9, 9, 9)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(typeField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 135, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 25, Short.MAX_VALUE)
                        .add(typeButton))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                        .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 119, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(findButton)
                            .add(removeButton))))
                .addContainerGap())
            .add(jPanel1Layout.createSequentialGroup()
                .add(jLabel1)
                .add(4, 4, 4)
                .add(jSpinner1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 91, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(isRecordingLabel)
                    .add(jLabel2))
                .add(58, 58, 58))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel1)
                    .add(jSpinner1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(26, 26, 26)
                        .add(isRecordingLabel)
                        .add(26, 26, 26)
                        .add(removeButton)
                        .add(18, 18, 18)
                        .add(findButton))
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(8, 8, 8)
                        .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 160, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 14, Short.MAX_VALUE)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(typeField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(typeButton)))
        );

        fileChooser2.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);

        fileMenu.setText("File");

        openMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        openMenuItem.setText("Open");
        openMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openMenuItem);

        saveMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        importMenuItem.setText("Import");
        importMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(importMenuItem);

        jMenuBar1.add(fileMenu);

        jMenu2.setText("Edit");

        addMenuItem.setText("Add");
        addMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(addMenuItem);

        addAtSelectedMenuItem.setText("Add at Selected");
        addAtSelectedMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAtSelectedMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(addAtSelectedMenuItem);

        editSelectedMenuItem.setText("Edit Selected");
        editSelectedMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editSelectedMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(editSelectedMenuItem);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_BACK_SPACE, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Remove Selected");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        clearMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_BACK_SPACE, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        clearMenuItem.setText("Clear");
        clearMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(clearMenuItem);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Run");

        runMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        runMenuItem.setText("Run All");
        runMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runMenuItemActionPerformed(evt);
            }
        });
        jMenu3.add(runMenuItem);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Continue");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem2);

        runSomeMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        runSomeMenuItem.setText("Run Selected");
        runSomeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runSomeMenuItemActionPerformed(evt);
            }
        });
        jMenu3.add(runSomeMenuItem);

        recordMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_SPACE, java.awt.event.InputEvent.CTRL_MASK));
        recordMenuItem.setText("Record");
        recordMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recordMenuItemActionPerformed(evt);
            }
        });
        jMenu3.add(recordMenuItem);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Find Selected");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem5);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
                .addContainerGap())
            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(layout.createSequentialGroup()
                    .addContainerGap()
                    .add(fileChooser, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(217, Short.MAX_VALUE)))
            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(layout.createSequentialGroup()
                    .addContainerGap()
                    .add(fileChooser2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(231, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(layout.createSequentialGroup()
                    .addContainerGap()
                    .add(fileChooser, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(209, Short.MAX_VALUE)))
            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(layout.createSequentialGroup()
                    .addContainerGap()
                    .add(fileChooser2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 13, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(207, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void runMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runMenuItemActionPerformed
        // TODO add your handling code here:
        run(((Integer)jSpinner1.getValue()).intValue(),0, al.size());
        
    }//GEN-LAST:event_runMenuItemActionPerformed

    public boolean changeRecording()
    {
        if(recording)
        {
            recording=false;     
            onTop=false;
            isRecordingLabel.setText("Stop");
            isRecordingLabel.setForeground(Color.green);
            watch.stop();
            prePauseTime+=(watch.getElapsedTime());
            
            
            //AlwaysOnTop.setState(false);
        }
        else
        {
            recording=true;
            onTop=true;
            isRecordingLabel.setText("Rec");
            isRecordingLabel.setForeground(Color.red);
            if(al.size()!=0)
            {
                //System.out.println(watch.getElapsedTime());
                watch.start();
                
                //System.out.println(watch.getElapsedTime());
            }
            //AlwaysOnTop.setState(true);
        }
        return recording;
    }
    private void recordMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recordMenuItemActionPerformed
        // TODO addf ydour handling code here:
        changeRecording();
        
        
    }//GEN-LAST:event_recordMenuItemActionPerformed

    public void clear()
    {
        al=new ArrayList<ClickPoint>();
        model.removeAllElements();//LinkedList<ClickPoint>();
        watch.stop();
        prePauseTime=0;
    }
    private void clearMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearMenuItemActionPerformed
        // TODO add your handling code here:
        clear();
    }//GEN-LAST:event_clearMenuItemActionPerformed

    public void importFile(File file)
    {
        try {
            /*try {
            textArea.read( new FileReader( file.getAbsolutePath() ), null );
            if(testing) System.out.println(textArea.getText());
            } catch (IOException ex) {
            Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            //try {
            // What to do with the file, e.g. display it in a TextArea]
            String line = "";
            FileReader fr = new FileReader(file);
            try {
                int c = fr.read();
                /*while(c>(int)'9'||c<(int)'0')
                {
                //if(testing) System.out.println(c);
                c=fr.read();
                }*/

                while (c != -1) {
                    c = fr.read();
                    //if(c!=124) c+=48;
                    line += (char) c;
                }
                if (testing) {
                    System.out.println(line);
                }
                // if(testing) System.out.println(line.indexOf("{"));
                // if(testing) System.out.println(line.indexOf("}"));

                line = line.substring(line.indexOf("{") + 1, line.indexOf("}"));
                //if(testing) System.out.println(line);

                for (int i = 0; i < line.length(); i++) {
                    /*if(Character.isWhitespace(line.charAt(i)))
                    {
                    line=line.substring(0,i)+line.substring(i+1);
                    }*/
                    /*if(!(Character.isLetterOrDigit(line.charAt(i)) ||line.charAt(i)=='['||line.charAt(i)==']'||line.charAt(i)=='|'||line.charAt(i)=='('||line.charAt(i)==')'||line.charAt(i)==','))
                    {
                    line=line.substring(0,i)+line.substring(i+1);
                    }*/
                }
                //if(testing) System.out.println(line);


                while (line.indexOf("[") != -1) {
                    //if(testing) System.out.println(line.substring(line.indexOf("["),line.indexOf("]")));
                    al.add(new ClickPoint(line.substring(line.indexOf("[") + 1, line.indexOf("]"))));
                    model.add(model.getSize(), al.get(al.size() - 1).toString());
                    line = line.substring(line.indexOf("]") + 1);
                    //if(testing) System.out.println(line);
                }

                //if(testing) System.out.println((char)(fr.read()+48));
            } catch (IOException ex) {
                Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
            }
            /*Scanner s=new Scanner(file.getAbsoluteFile());
            String next=s.nextLine();
            if(testing) System.out.println(next.substring(0, next.indexOf("|")));*/

            //} catch (IOException ex) {
            //}
            //}
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void importMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importMenuItemActionPerformed
        // TODO add your handling code here:
        int returnVal = fileChooser.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = fileChooser.getSelectedFile();
        importFile(file);
    } else {
        System.out.println("File access cancelled by user.");
    }
        
    }//GEN-LAST:event_importMenuItemActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
        //if(testing) System.out.println("Stopped");
        int i=list.getSelectedIndex();
        ((MouseEmulator)e).moveTo((int) al.get(i).getX(), (int) al.get(i).getY(), 20);
        if(al.get(i).isTypePoint())
        {
            typeField.setText(al.get(i).getTyped());
        }
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void runSomeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runSomeMenuItemActionPerformed
        // TODO add your handling code here:
        
            
        run(1,list.getMinSelectionIndex(),list.getMaxSelectionIndex());
                
            
        
    }//GEN-LAST:event_runSomeMenuItemActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        // TODO add your handling code here:
        
        
        int returnVal = fileChooser2.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String points="{";
            
            for(int i=0;i<al.size(); i++)
            {
                points+=al.get(i);
            }
            points+="}";
            //if(testing) System.out.println("points.toString()="+points);
            FileWriter fw = null;
            try {
                File file = fileChooser2.getSelectedFile();
                fw = new FileWriter(file);
                fw.write(points);
               // if(testing) System.out.println(file.getAbsolutePath());
                    

            } catch (IOException ex) {
                Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fw.close();
                } catch (IOException ex) {
                    Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
                }
                }
        } else {
            System.out.println("File access cancelled by user.");
        }
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
        
        int[] sel=list.getSelectedIndices();
        for(int i=0; i<sel.length; i++)
        {
            al.remove(sel[sel.length-i-1]);
            model.remove(sel[sel.length-i-1]);
        }
       
    }//GEN-LAST:event_removeButtonActionPerformed

    private void findButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_findButtonActionPerformed
        int i=list.getSelectedIndex();
        ((MouseEmulator)e).moveTo((int) al.get(i).getX(), (int) al.get(i).getY(), 20);
        if(al.get(i).isTypePoint())
        {
            typeField.setText(al.get(i).getTyped());
        }
    }//GEN-LAST:event_findButtonActionPerformed

    private void listCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_listCaretPositionChanged

    }//GEN-LAST:event_listCaretPositionChanged

    private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuItemActionPerformed
        // TODO add your handling code here:
        clearMenuItemActionPerformed(evt);
        importMenuItemActionPerformed(evt);
    }//GEN-LAST:event_openMenuItemActionPerformed

    private void typeFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_typeFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_typeFieldActionPerformed

    private void typeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_typeButtonActionPerformed

        if (list.getSelectedIndex() != -1) {             al.get(list.getSelectedIndex()).setTyped(typeField.getText());         } else {             String s = typeField.getText();             //kEmulator.type(s);             int x, y;             x = (int) MouseInfo.getPointerInfo().getLocation().getX();             y = (int) MouseInfo.getPointerInfo().getLocation().getY();             //System.out.println(x+","+y);             //             //System.out.println(event.getID());             if (recording) {                 long time = 0;                  //if((x>276||y>234))                 //{                 if (watch.isRunning()) {                     watch.stop();                     time = watch.getElapsedTime();                 }                  //recording=false;                 //onTop=false;                 //kEmulator.type(8);                 if (list.getSelectedIndex() == -1) {                     e.jumpTo(al.get(al.size() - 1).getX(), al.get(al.size() - 1).getY());                 } else {                     e.jumpTo(al.get(list.getSelectedIndex()).getX(), al.get(list.getSelectedIndex()).getY());                 }                 e.click();                 //e.wait(500);                 //e.click();                  kEmulator.type(s);                  al.add(new ClickPoint(x, y, time, s));                 model.add(model.getSize(), al.get(al.size() - 1).toFormatString());                 watch.start();                 System.out.println("Key Pressed");                 //refocus(x,y);                  //jSpinner1.setValue(1);                 //recording=                 //onTop=true;                 //refocus(x,y);                 //}             }         }      }//GEN-LAST:event_typeButtonActionPerformed
        }
    }
        private void editSelectedMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editSelectedMenuItemActionPerformed
        // TODO add your handling code here:
        if(list.getSelectedIndex()==-1) return;
        jFrame1.setVisible(true);
        jFrame1.setSize(301, 186);
        xVal.setValue(new Integer(al.get(list.getSelectedIndex()).getX()));
        yVal.setValue(new Integer(al.get(list.getSelectedIndex()).getY()));
        waitVal.setValue(new Integer(al.get(list.getSelectedIndex()).getWaitTime()));
        textField.setText(al.get(list.getSelectedIndex()).getTyped());
    }//GEN-LAST:event_editSelectedMenuItemActionPerformed

    private void doneButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doneButtonActionPerformed
        // TODO add your handling code here:
        if(testing) System.out.println(xVal.getValue());
        if(testing) System.out.println(yVal.getValue());
        if(testing) System.out.println(xVal.getValue());
        al.get(list.getSelectedIndex()).setX((new Integer(xVal.getValue().toString())).intValue());
        al.get(list.getSelectedIndex()).setY((new Integer(yVal.getValue().toString())).intValue());
        al.get(list.getSelectedIndex()).setWait((new Integer(waitVal.getValue().toString())).intValue());
        al.get(list.getSelectedIndex()).setTyped(textField.getText());
        model.setElementAt(al.get(list.getSelectedIndex()).toFormatString(), list.getSelectedIndex());
        if(testing) System.out.println(jFrame1.getSize().toString());
        jFrame1.setVisible(false);
        //if(methodRadioButton.isSelected())
        //{
        //    al.get(list.getSelectedIndex()).setIsMethodCall(true);
        //    al.get(list.getSelectedIndex()).setMethod(classTextField.getText(), methodTextField.getText());
       // }
        
        
    }//GEN-LAST:event_doneButtonActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        int[] sel=list.getSelectedIndices();
        for(int i=0; i<sel.length; i++)
        {
            al.remove(sel[sel.length-i-1]);
            model.remove(sel[sel.length-i-1]);
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void addAtSelectedMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAtSelectedMenuItemActionPerformed
        // TODO add your handling code here:
        int selNum=list.getSelectedIndex();
        if(selNum==-1)
        {
            selNum=al.size()-1;
        }
        
        selNum++;
        al.add(selNum,new ClickPoint(0,0,0,""));
        
        model.add(selNum, al.get(selNum).toFormatString());
        list.setSelectedIndex(selNum);
        editSelectedMenuItemActionPerformed(evt);
    }//GEN-LAST:event_addAtSelectedMenuItemActionPerformed

    private void addMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addMenuItemActionPerformed
        // TODO add your handling code here:
        int selNum=al.size();
        al.add(selNum,new ClickPoint(0,0,0,""));
        
        model.add(selNum, al.get(selNum).toFormatString());
        list.setSelectedIndex(selNum);
        editSelectedMenuItemActionPerformed(evt);
    }//GEN-LAST:event_addMenuItemActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        run(1,list.getSelectedIndex(),al.size());
    }//GEN-LAST:event_jMenuItem2ActionPerformed
  
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Driver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Driver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Driver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Driver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Driver()).setVisible(true);
                //Test t=new Test();
                //t.setLocation(new Point(1550,25));
                //t.setVisible(true);
                
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addAtSelectedMenuItem;
    private javax.swing.JMenuItem addMenuItem;
    private javax.swing.JMenuItem clearMenuItem;
    private javax.swing.JButton doneButton;
    private javax.swing.JMenuItem editSelectedMenuItem;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JFileChooser fileChooser2;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JButton findButton;
    private javax.swing.JMenuItem importMenuItem;
    private javax.swing.JLabel isRecordingLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton4;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JList list;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JCheckBoxMenuItem recordMenuItem;
    private javax.swing.JButton removeButton;
    private javax.swing.JMenuItem runMenuItem;
    private javax.swing.JMenuItem runSomeMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JTextField textField;
    private javax.swing.JButton typeButton;
    private javax.swing.JTextField typeField;
    private javax.swing.JSpinner waitVal;
    private javax.swing.JSpinner xVal;
    private javax.swing.JSpinner yVal;
    // End of variables declaration//GEN-END:variables

   
    public void keyTyped(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet.");
        //if(testing) System.out.println("Key Pressed");
        
    }

    
    public void keyPressed(KeyEvent ke) {
        //if(testing) System.out.println("Key Pressed");
    }

  
    public void keyReleased(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet.");
        //if(testing) System.out.println("Key Pressed");
        
    }
}
