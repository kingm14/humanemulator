package newpackage3;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kingm14
 */
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.JLabel;
import java.awt.event.*;
import java.awt.Robot;

public class Emulator {
    protected Robot robot;
    public Emulator()
    {
        try {
            robot=new Robot();
        } catch (AWTException ex) {
            Logger.getLogger(Emulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void wait(int i){robot.delay(i);}
}
