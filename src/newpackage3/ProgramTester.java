package newpackage3;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kingm14
 */
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.JLabel;
import java.awt.event.*;
import java.awt.Robot;
import java.util.*;
import java.io.*;
public class ProgramTester {
    
    private MouseEmulator e;
    private ArrayList<ClickPoint> al;
    private KeyEmulator kEmulator;
    private String arrowSymbols;
    private int numTimesRepeat;
    
    public ProgramTester()
    {
        e=new MouseEmulator();
        kEmulator=new KeyEmulator();
        al=new ArrayList<ClickPoint>();
        arrowSymbols="˂˄˃˅";
        numTimesRepeat=1;
        
    }
    public void addClickPoint(ClickPoint clickPoint, int i)
    {
        al.add(i, clickPoint);
    }
    public void addClickPoint(ClickPoint clickPoint)
    {
        addClickPoint(clickPoint, al.size());
    }
    public void setClickPoint(ClickPoint clickPoint, int i)
    {
        al.set(i, clickPoint);
    }
    public void removeClickPoint(int i)
    {
        al.remove(i);
    }
    public void removeClickPoint()
    {
        removeClickPoint(al.size()-1);
    }
    public void moveTo(int x, int y, int speed)
    {
        e.moveTo(x, y, speed);
    }
    public void clickMouse()
    {
        e.click();
    }
    public void pressMouse()
    {
        e.press();
    }
    public void releaseMouse()
    {
        e.release();
    }
    public void refocus(int x, int y)
    {
        ((MouseEmulator)e).jumpTo(5, 50);
        ((MouseEmulator)e).click();
        ((MouseEmulator)e).click();
        ((MouseEmulator)e).jumpTo(x, y);
        
    }
    
            
}
